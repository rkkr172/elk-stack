GET /_cat/indices?v

## enrich-assets

PUT enrich-assets
PUT /enrich-assets/_mapping
{
  "properties": {
    "@timestamp": {
      "type" : "date",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      },
    "field" : {
      "type" : "text",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
          },
    "ip" : {
      "type" : "ip",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      },
    "name" : {
        "type" : "text",
        "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      }
    }
}


GET /enrich-assets


#2. creating a enrichment policy

PUT /_enrich/policy/enrich-ip-policy
{
  "match": {
    "indices": ["enrich-assets"],
    "match_field": "ip",
    "enrich_fields": ["name","field"]
  }
}

PUT /_enrich/policy/enrich-name-policy
{
  "match": {
    "indices": ["enrich-assets"],
    "match_field": "name",
    "enrich_fields": ["ip","field"]
  }
}


#3. uploading data into enrichment index (using logstash pipeline)
#/etc/logstash/conf.d/asset-enrichment.conf
input {
  file {
    path => "/var/log/enrichment/asset_*.csv",
    mode => "read",
    start_position => "beginning"
  }
}
filter {
  grok {
    match => { "message" => "(%{IP:ip})?;%{GREEDYDATA:name};%{GREEDYDATA:field}" }
  }
}
mutate {
  remove_field => ["path","host","message"]
  lowercase => ["name","ip","field"]
  split => { "field" => "#" }
}

output {
  elasticsearch {
    hosts => []
    index => "enrich-assets"
    #user =>
    #password
    #ssl =>
    #ssl_certificate_verification => false
    #cacert => 
  }
}


#4. activate enrichment policy
POST /_enrich/policy/enrich-ip-policy/_execute
POST /_enrich/policy/enrich-name-policy/_execute

GET /.enrich-enrich-ip-policy
GET /.enrich-enrich-name-policy

#5. create ingest pipeline for source,destination,host
#common ingest pipeline with assets range
