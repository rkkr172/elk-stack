apt install syslog-ng -y

apt-get install librdkafka1 librdkafka-dev python3-pip -y
ldconfig -vvv

apt install python3-pip -y
pip install syslogng_kafka

- syslog-ng --version   ( Version checking)
- syslog-ng -s          ( Sentex checking)

Step-1: Package Installation
-----------------------------
root@deb-63:~# apt install syslog-ng
root@deb-63:~# ls /etc/syslog-ng/
conf.d  patterndb.d  scl.conf  syslog-ng.conf

Step-2: configuration
----------------------
# less /etc/syslog-ng/syslog-ng.conf

options {
        flush_lines(0);
        time_reopen (10);
        log_fifo_size (1000);
        chain_hostnames(off);
        use_dns(no);
        use_fqdn(no);
        create_dirs (no);
        keep_hostname (yes);
        #owner("root");
        #group("adm");
        #perm(0640);
        #stats_freq(0);
        #bad_hostname("^gconfd$");
};
# Sources
########################
source s_unix_something_u { udp (ip(192.168.56.63) port(514));};

# Destinations
########################
destination d_unix_logstash_u { udp("192.168.56.64" port(5044) ); };    ## Log forwarding to Logstash node-64

# Filters
########################

# Log paths
########################
#log { source(s_src); destination(d_net); };
log { source(s_unix_something_u); destination(d_unix_logstash_u); };    ## message sent to remote machine node-64

Step-3: Allow port
---------------------------------
root@deb-63:~# ufw allow 514/udp
root@deb-63:~# ufw reload

Step-4: Service enable
---------------------------------
root@deb-63:~# systemctl start rsyslog.Service

12:27:53.860984 IP 192.168.56.62.60744 > 192.168.56.63.514: UDP, length 126
12:27:53.862535 IP 192.168.56.63.47803 > 192.168.56.64.5044: UDP, length 127
12:27:56.660993 IP 192.168.56.62.60744 > 192.168.56.63.514: UDP, length 57
12:27:56.661402 IP 192.168.56.63.47803 > 192.168.56.64.5044: UDP, length 58
12:28:02.671180 IP 192.168.56.62.60744 > 192.168.56.63.514: UDP, length 57
12:28:02.672318 IP 192.168.56.63.47803 > 192.168.56.64.5044: UDP, length 58
12:28:04.108786 IP 192.168.56.62.60744 > 192.168.56.63.514: UDP, length 127
12:28:04.109840 IP 192.168.56.63.47803 > 192.168.56.64.5044: UDP, length 128
^C
453 packets captured
453 packets received by filter
0 packets dropped by kernel
root@deb-63:~#


https://www.syslog-ng.com/community/b/blog/posts/kafka-destination-improved-with-template-support-in-syslog-ng

destination d_kafka {
  kafka-c(config(metadata.broker.list("192.168.56.77:9092")
        queue.buffering.max.ms("1000"))
        topic("unix-to-kafka-logs")
        message("$(format-json --scope rfc5424 --scope nv-pairs)"));
};

log {
  source(src);
  destination(d_kafka);
};


- https://syslogng-kafka.readthedocs.io/en/latest/installation.html

root@deb-63:~# cat /etc/syslog-ng/conf.d/syslog-to-kafka-py.conf
destination syslog_to_kafka {
    python(
        class("syslogng_kafka.kafkadriver.KafkaDestination")
            on-error("fallback-to-string")
            options(
                hosts("192.168.56.77:9092")
                topic("unix-to-kafka-logs")
                #partition("10")
                msg_key("src_ip")
                #programs("firewall,nat")
                #broker_version("0.8.2.1")
                verbose("True")
                display_stats("True")
#producer_config("{'client.id': 'sylog-ng-01', 'retry.backoff.ms': 100, 'message.send.max.retries': 5, 'queue.buffering.max.kbytes': 50240, 'default.topic.config': {'request.required.acks': 1, 'request.timeout.ms': 5000, 'message.timeout.ms': 300000}, 'queue.buffering.max.messages': 100000, 'queue.buffering.max.ms': 1000, 'statistics.interval.ms': 15000, 'socket.timeout.ms': 60000, 'retry.backoff.ms':100,}")
)
);
};
log {
    source(s_src);
    destination(syslog_to_kafka);
};
