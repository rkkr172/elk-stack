apt install gnupg
apt install apt-transport-https

wget -q0 - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee /etc/apt/sources.list.d/elastic-7.x.list

apt install elasticsearch

# ------------------------------------ Node ------------------------------------
node.name: node-1

# ---------------------------------- Network -----------------------------------
#network.host: 192.168.0.1
network.host: 0.0.0.0

# --------------------------------- Discovery ----------------------------------
#discovery.seed_hosts: ["host1", "host2"]
discovery.seed_hosts: ["192.168.56.75"]

# Bootstrap the cluster using an initial set of master-eligible nodes:
#cluster.initial_master_nodes: ["node-1", "node-2"]
cluster.initial_master_nodes: ["node-1"]

:wq!

root@deb-75:~# systemctl daemon-reload
root@deb-75:~# systemctl start elasticsearch

# shakespeare-mapping
"speaker" : {"type": "keyword" },
"play_name" : {"type": "keyword" },
"line_id" : { "type" : "integer" },
"speech_numbe  r" : { "type" : "integer" }


## enrich-assets

1. Creating a mapping

PUT enrich-assets

PUT /enrich-assets/_mapping
{
  "properties": {
    "@timestamp": {
      "type" : "date",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      },
    "field" : {
      "type" : "text",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
          },
    "ip" : {
      "type" : "ip",
      "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      },
    "name" : {
        "type" : "text",
        "fields" : {
  "keyword" : {
    "type": "keyword",
    "ignore_above" : 256
  }
}
      }
    }
}



GET /enrich-assets
DELETE /enrich-assets

curl -H "Conten-Type: application/json" -XGET "http://localhost:9200/"
curl -H "Conten-Type: application/json" -XGET "http://localhost:9200/_cat/indices?pretty"