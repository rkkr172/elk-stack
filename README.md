## ELK Stack

- https://sundog-education.com/elasticsearch/
- https://notepad.pw/adcdefg

Flow Diagram

1. Log Forwarding maybe configured
- rsyslog(62) ==>> Syslog-ng Collector(63) ==>> logstash(64) ==>>  elasticsearch(75) ==>> kibana(76)
- rsyslog(62) ==>> Syslog-ng Collector(63) ==>> Logstash(64) ==>> Kafka(77) ==>>  elasticsearch(75) ==>> kibana(76)

2. Lab OS (Operating System)
- https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/
- https://wiki.debian.org/SourcesList
- Syslog(unix)        x.x.x.62
- syslog-ng Collector x.x.x.63
- logstash            x.x.x.64
- elasticsearch       x.x.x.75
- kibana              x.x.x.76
- kafka               x.x.x.77

3. Default Service Ports which using in lab setup:
- syslog --> 514
- logstash --> 5400
- elasticsearch --> 9200
- kibana --> 5601
- bootstrap -->	9092
- zookeeper -->	2181
- kafka --> 9000

# syslog-ng
- syslog-ng --version   ( Version checking)
- syslog-ng -s          ( Sentex checking)
- https://github.com/rsyslog/rsyslog-doc/blob/master/README.md
- http://saalwaechter-notes.blogspot.com/2018/02/rsyslog-and-markmessageperiod.html
- https://www.rsyslog.com/doc/master/tutorials/log_sampling.html
- https://coralogix.com/blog/cdn-logs-why-you-need-them/
- https://www.keycdn.com/support/configure-syslog-server
- https://www.tecmint.com/create-centralized-log-server-with-rsyslog-in-centos-7/
- https://www.linuxtechi.com/setup-rsyslog-server-on-debian/
- https://www.syslog-ng.com/community/b/blog/posts/kafka-destination-improved-with-template-support-in-syslog-ng
- https://www.syslog-ng.com/technical-documents/doc/syslog-ng-open-source-edition/3.16/administration-guide/36
- https://www.syslog-ng.com/technical-documents/doc/syslog-ng-open-source-edition/3.30/administration-guide/
$ModLoad immark
$MarkMessagePeriod 600
module(load="immark" MarkMessagePeriod="600")
module(load="immark" interval="600")
***

[ Filebeat ]

***

[ Logstash ]
- https://www.elastic.co/logstash/
- https://www.elastic.co/guide/en/logstash/current/index.html
- https://www.elastic.co/guide/en/logstash-versioned-plugins/current/index.html
***

[ Kafka ]
- https://www.apache.org/dyn/closer.cgi?path=/kafka/3.1.0/kafka_2.13-3.1.0.tgz	#download
- https://www.elastic.co/guide/en/logstash/current/plugins-inputs-kafka.html	#input configuration
- https://kafka.apache.org/25/documentation.html#brokerconfigs
- https://kafka.apache.org/documentation.html#connect
- https://www.confluent.io/blog/kafka-elasticsearch-connector-tutorial/
- https://sematext.com/blog/kafka-connect-elasticsearch-how-to/
- https://sematext.com/blog/logstash-alternatives/

=> Starting zookeeper and kafka server
/root/kafka213/bin/zookeeper-server-start.sh /root/kafka213/config/zookeeper.properties
/root/kafka213/bin/kafka-server-start.sh /root/kafka213/config/server.properties

# After installation and Starting Services
=> Create TOPIC
./bin/kafka-topics.sh --bootstrap-server=192.168.56.77:9092 --create --topic test-topic

=> PRODUCE TOPIC
./bin/kafka-console-producer.sh --bootstrap-server=192.168.56.77:9092 --topic test-topic

=> CONSUME from topic
./bin/kafka-console-consumer.sh --bootstrap-server=192.168.56.77:9092 --topic test-topic --from-beginning


[ Kibana ]


------------

[ Elasticsearch ]

1. References:
- https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
- https://www.elastic.co/guide/en/elasticsearch/reference/7.16/security-minimal-setup.html
- https://www.elastic.co/guide/en/elasticsearch/reference/current/set-up-a-data-stream.html
- https://coralogix.com/blog/elasticsearch-disk-and-data-storage-optimizations-with-benchmarks/ #Optimization

2. Elasticsearch 7 Repository configurations:
- wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
- echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee /etc/apt/sources.list.d/elastic-7.x.list
- apt-get install apt-transport-https

3. Some Helpful Commands:
- http://localhost:9200/_cat/shards?v
- http://localhost:9200/_cat/shards?v&s=state:desc
- http://localhost:9200/_cat/indices?v
- http://localhost:9200/_cat/indices/?pretty&s=store.size:desc
- http://localhost:9200/_cluster/health?pretty
- http://localhost:9200/_cluster/allocation/explain?pretty

https://www.subpng.com/png-g5zllz/
https://www.1337xx.to/