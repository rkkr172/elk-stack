https://www.elastic.co/guide/en/logstash/current/installing-logstash.html
https://devconnected.com/how-to-install-logstash-on-ubuntu-18-04-and-debian-9/
https://www.tutorialspoint.com/logstash/logstash_installation.htm

apt-get install apt-transport-https

echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

sudo apt-get update && sudo apt-get install logstash

/usr/share/logstash/bin/logstash-plugin list