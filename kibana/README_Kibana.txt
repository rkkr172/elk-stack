server.port: 5601
server.host: "192.168.56.76"
server.name: "deb76-kibana"
elasticsearch.hosts: ["http://192.168.56.75:9200"]

==========================
# Configure Index Pattern
==========================
Management ---> kibana (Index Patterns) ---> Create Index Pattern --->
    Name: movie*
    Timestamp field: year
    [Create Index Pattern]

Analytics ---> Discover  ---> choose Pattern index and search data

==========================
# 
==========================