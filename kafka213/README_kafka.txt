=> Deploying kafka & CMAK with ELK
- Apache kafka launch their commercial product of kafka called Confluent. It offer the user a GUI configure and monitor their kafka infrastructure.
- Flow charts
[beats-1, beats-2, beats-3, ...] --> kafka ---> Logstash ---> [elasticsearch-1, elasticsearch-2] <--- Kibana

- Reference URLs
https://kafka.apache.org/downloads
https://www.apache.org/dyn/closer.cgi?path=/kafka/3.1.0/kafka-3.1.0-src.tgz
DOC-1385158

root@deb77:/usr/local/kafka# bin/zookeeper-server-start.sh config/zookeeper.properties &
root@deb77:/usr/local/kafka# bin/kafka-server-start.sh config/server.properties &

root@deb77:/usr/local/kafka# bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic unix
Created topic unix.

==========================
# kafka installation
==========================
wget https://dlcdn.apache.org/kafka/3.1.0/kafka_2.13-3.1.0.tgz
tar -xzf kafka_2.13-3.1.0.tgz
mv kafka_2.13-3.1.0/ kafka213
cd /tmp/kafka213

mkdir -p /root/data/kafka /root/data/zookeeper
cp zookeeper.properties zookeeper.properties.old
cp server.properties server.properties.old
vim zookeeper.properties
vim server.properties

#====================================================
# Starting zookeeper and kafka server
#====================================================
/tmp/kafka213/bin/zookeeper-server-start.sh config/zookeeper.properties
/tmp/kafka213/bin/kafka-server-start.sh config/server.properties

/root/kafka213/bin/zookeeper-server-start.sh /root/kafka213/config/zookeeper.properties
/root/kafka213/bin/kafka-server-start.sh /root/kafka213/config/server.properties
#====================================================
# After installation and Starting Services
#====================================================
=> Create TOPIC 
bin/kafka-topics.sh --bootstrap-server=192.168.56.77:9092 --create --topic test-topic

=> PRODUCE TOPIC
bin/kafka-console-producer.sh --bootstrap-server=192.168.56.77:9092 --topic test-topic

=> CONSUME from topic
./bin/kafka-console-consumer.sh --bootstrap-server=192.168.56.77:9092 --topic test-topic --from-beginning

=> Logstash logs
bin/kafka-console-consumer.sh --bootstrap-server=192.168.56.77:9092 --topic unix-kafka-logs



==========================
# CMAK installation
==========================
- https://github.com/yahoo/CMAK
- https://sleeplessbeastie.eu/2021/08/04/how-to-install-cluster-manager-for-apache-kafka/

- https://computingforgeeks.com/setup-apache-kafka-with-cmak-on-centos/
- https://computingforgeeks.com/configure-apache-kafka-on-ubuntu/
- https://www.scala-sbt.org/0.13/docs/Library-Management.html#Eviction+warning

apt install git
git clone https://github.com/yahoo/CMAK.git
cd CMAK/conf
cp application.conf application.conf.old
vim application.conf
cd ..
./sbt clean dist


kafkaAdminClientThreadPoolSize = 2
brokerViewThreadPoolSize = 2
offsetCacheThreadPoolSize
kafkaAdminClientThreadPoolSize


==========================
# syslog-ng to interac with kafka - works only on debian9
==========================
Prerequisites: https://syslogng-kafka.readthedocs.io/en/latest/index.html
--------------
For this scenario I used the python library syslogng_kafka

apt install syslog-ng-mod-python python-pip librdkafka-dev
pip install syslogng_kafka

export {http,https}_proxy=http://1.2.3.4:3128


=> Edit the Kafka driver to dump JSON messages (it will allow us to replace ' with ", that launch an error when json codec is used in logstash)
/usr/local/lib/python2.7/dist-packages/syslogng_kafka/kafkadriver.py
Add line at begining:   import json
Change line 210 : msg_string = str(msg) with msg_string = json.dumps(msg)

=> updating file "/etc/syslog-ng/syslog-ng.conf"
## configing kafka destination
destination unix_to_kafka { 
python(
        class("syslogng_kafka.kafkadriver.KafkaDestination")
        on-error("fallback-to-string")
        options(
        hosts("192.168.56.77:9200")
        topic("unix")
        msg_key("src_ip")
        )
        );
};


root@deb77:~# cat /etc/systemd/system/zookeeper.service
[Unit]
Description=Apache Zookeeper Server(Kafka)
#Documentation=http://zookeeper.apache.org
Require=network.target remote-fs.target
After=network.target remore-fs.target
[Service]
Type=simple
Environment=/usr/lib/jvm/default-java/bin/
ExecStart=/usr/local/kafka/bin/zookeeper-server-start.sh /usr/local/kafka/config/zookeeper.properties
ExecStop=/usr/local/kafka/bin/zookeeper-server-stop.sh
ExecReload=/bin/kill -HUP $MAINPID
StandardOutput=journal
StandardError=journal
[Install]
WantedBy=multi-user.target


root@deb77:~# cat /etc/systemd/system/kafka.service
[Unit]
Description=Kafka Server (Please verify zookeeper must be started)
#Documentation=http://kafka.apache.org/documentation.html
Require=network.target remote-fs.target
After=network.target remore-fs.target
[Service]
Type=simple
Environment=/usr/lib/jvm/default-java/bin/
ExecStart=/usr/local/kafka/bin/kafka-server-start.sh /usr/local/kafka/config/server.properties
ExecStop=/usr/local/kafka/bin/kafka-server-stop.sh
ExecReload=/bin/kill -HUP $MAINPID
StandardOutput=journal
StandardError=journal
[Install]
WantedBy=multi-user.target